
//////////////////////////////////////////////////////////////////////////////AJAX
function update_doc(url, cfunc, type, post) {
    var xhttp;
    xhttp=new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
			if(cfunc != "")
			{
				if(post == "")//ja nav noteikti citi parametri
				{
					cfunc(xhttp);
				}
				else
				{
					cfunc(xhttp, post);
				}
			}
        }
    };
	
	xhttp.open(type, url, true);

	if(type == "GET")
	{
		xhttp.send();
	}
	else
	{
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send(post);
	}

}

//////////////////////////////////////////////////////////////////////////////Filmu saraksts
function check_imdb(page)//Izsauc ajax
{
	title = document.getElementById("searc_id").value;
	update_doc('http://www.omdbapi.com/?s='+title+'&plot=full&r=json&page='+page+'', update_data, "POST", page);
}


function update_data(xhttp, page)// Apstrādā un attēlo filmu sarakstu
{
	parameters = JSON.parse(xhttp.responseText);
	if(parameters.Response == "True")
	{
		pages=Math.ceil(parameters.totalResults / 10);
		document.getElementById("content_nf").style = "visibility:hidden";
		document.getElementById("content").innerHTML="";
		for(i = 0; i < parameters.Search.length; i++)
			{
				movie_box = document.createElement("div");
				movie_box.setAttribute("class", "movie_box");
				if(parameters.Search[i].Poster != "N/A")
				{
					movie_box_html = "<div id='movie_box_poster'><img id='content_pic' height='140px' width='95px' src='"+parameters.Search[i].Poster+"'></div>";
				}
				else
				{
					movie_box_html = "<div id='movie_box_poster'><img id='content_pic' height='140px' width='95px' src='images/no_picture.png'></div>";
				}
				title  = parameters.Search[i].Title.substring(0, 35);
				if(title.length > 34){title += "...";}
				movie_box_html+="<span class='movie_box_title'>"+title+"</span>";
				movie_box_html+="<span class='movie_box_description'><b>Year: </b>"+parameters.Search[i].Year+"</span>";
				movie_box_html+="<span class='movie_box_description'><b>Type: </b>"+parameters.Search[i].Type+"</span>";
				movie_box_html+="<div id='more_info_button' onclick='open_one(&quot;"+parameters.Search[i].imdbID+"&quot;, "+page+")'></div>";
				movie_box.innerHTML=movie_box_html;				
				document.getElementById("content").appendChild(movie_box);
			}
		if(pages>1)
		{
			document.getElementById("content").innerHTML+="<div id='navigation_menu'>";			
			if(page != 1) {document.getElementById("content").innerHTML+="<span class='navigation' onclick='check_imdb(1)'>1<<</span>";}
			if(page != 1 && page != 2) {document.getElementById("content").innerHTML+="<span class='navigation' onclick='check_imdb("+(page - 2)+")'>"+(page - 2)+"</span>";}
			if(page != 1) {document.getElementById("content").innerHTML+="<span class='navigation' onclick='check_imdb("+(page - 1)+")'>"+(page - 1)+"</span>";}
			document.getElementById("content").innerHTML+="<span class='navigation_current'>"+page+"</span>";
			if(page != pages) {document.getElementById("content").innerHTML+="<span class='navigation' onclick='check_imdb("+(page + 1)+")'>"+(page + 1)+"</span>";}
			if(page != pages && page != (pages - 1)) {document.getElementById("content").innerHTML+="<span class='navigation' onclick='check_imdb("+(page + 2)+")'>"+(page + 2)+"</span>";}
			if(page != pages) {document.getElementById("content").innerHTML+="<span class='navigation' onclick='check_imdb("+pages.toString()+")'>>>"+pages.toString()+"</span>";}
			document.getElementById("content").innerHTML+="</div>";
		}
		
	}
	else
	{
		document.getElementById("content_nf").style = "visibility:visible";
	}
}

//////////////////////////////////////////////////////////////////////////////Filmas apskats
function open_one(id, page)//Izsauc ajax
{
	update_doc('http://www.omdbapi.com/?i='+id+'&plot=full&r=json', update_one, "POST", page);
}

function update_one(xhttp, page)// Apstrādā un attēlo izvēlētās filmas sarakstu...
{
parameters = JSON.parse(xhttp.responseText);
	if(parameters.Response == "True")
	{	
		elem_html="";
		if(parameters.Poster != "N/A")
		{
			elem_html+="<div id='content_poster'><img id='content_pic' height='438px' width='300px' src='"+parameters.Poster+"'></div>";
		}
		else
		{
			elem_html+="<div id='content_poster'><img id='content_pic' height='438px' width='300px' src='images/no_picture.png'></div>";
		}
		
		elem_html+="<div id='content_title'>"+parameters.Title+"</div>";
		elem_html+="<span class='content_other'><b>Year:</b>"+parameters.Year+"</span>";		
		elem_html+="<span class='content_other'><b>Genre:</b>"+parameters.Genre+"</span>";
		elem_html+="<span class='content_other'><b>Runtime:</b>"+parameters.Runtime+"</span>";
		elem_html+="<span class='content_other'><b>Released:</b>"+parameters.Released+"</span>";
		elem_html+="<span class='content_other'><b>Director:</b>"+parameters.Director+"</span>";
		elem_html+="<span class='content_other'><b>Actors:</b>"+parameters.Actors+"</span>";
		elem_html+="<span class='content_other'><b>Rated:</b>"+parameters.Rated+"</span>";
		elem_html+="<span class='content_other'><b>Language:</b>"+parameters.Language+"</span>";
		elem_html+="<span class='content_other'><b>Country:</b>"+parameters.Country+"</span>";
		elem_html+="<span class='content_other'><b>IMDB rating:</b>"+parameters.imdbRating+"</span>";
		elem_html+="<span class='content_other'><b>IMDB votes:</b>"+parameters.imdbVotes+"</span>";
		elem_html+="<span class='content_other'><b>Plot:</b>"+parameters.Plot+"</span>";
		elem_html+="<div id='back_butt' onclick='check_imdb("+page+")'></div>";
		document.getElementById("content").innerHTML=elem_html;
	}
}