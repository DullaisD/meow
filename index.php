<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Meow ?</title>
		<link rel="stylesheet" href="styles.css" type="text/css" />
		<script src="js.js"></script>
	</head>
	<body>
		<div id="container">
			<div id="header">
				<div id="logo"></div>
				<span class="search_text">Search IMDB database:</span>
				<div id="search_field"><input  type="text" id="searc_id" class="field" onkeyup="check_imdb(1)"></div>
				<span id="content_nf">No movies found !</span>
			</div>
			<div id="body">
				<div id="content">			
					<div id="content_poster"><img id="content_pic" height="438px" width="300px" src="images/no_picture.png"></div>
					<div id="content_title">Enter movie name to retrieve information !</div>
					<span id="content_year" class="content_other"></span>
					<span id="content_genre" class="content_other"></span>
					<span id="content_runtime" class="content_other"></span>
					<span id="content_released" class="content_other"></span>
					<span id="content_director" class="content_other"></span>
					<span id="content_actors" class="content_other"></span>
					<span id="content_rated" class="content_other"></span>
					<span id="content_language" class="content_other"></span>
					<span id="content_country" class="content_other"></span>
					<span id="content_imdbrating" class="content_other"></span>
					<span id="content_imdbvotes" class="content_other"></span>
					<span id="content_plot" class="content_other"></span>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer">
				<div class="footer-bottom">
					<p>&copy; "Nē, tu saki "kakis",bet jāsaka "kakis"!" 2016</p>
				 </div>
			
			</div>
		</div>
	</body>
</html>
